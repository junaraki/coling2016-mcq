#!/usr/bin/env python

from pattern.en import singularize
import argparse

"""
Python 2 code to use pattern.en to singularize a noun.

Example:
$ python2.7 singularize.py apples
apple
"""

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('noun', type=str, help='noun to be singularized')
    args = parser.parse_args()
    print singularize(args.noun)
