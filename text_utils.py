#! /usr/bin/env python

from nltk.corpus import stopwords
import string
import itertools
import operator
import string

Stopwords = stopwords.words("english")

def remove_null(list_input):
	return filter(None, list_input)

def lower(list_input):
	return [x.lower() for x in list_input]

def upper(list_input):
	return [x.upper() for x in list_input]

def remove_stopwords(list_input):
	return [x for x in list_input if x not in Stopwords]

def remove_punctuation(list_input):
	return [x for x in list_input if x not in string.punctuation]	

def remove_single_letter_words(list_input):
	return [x for x in list_input if len(x) > 1]

def flatten_list(list_input):
	return [item for sublist in list_input for item in sublist]

def remove_numbers(list_input):
	return [x for x in list_input if x.isdigit() == False]