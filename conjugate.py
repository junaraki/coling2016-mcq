#!/usr/bin/env python

from pattern.en import conjugate
import argparse

"""
Python 2 code to use pattern.en to return a verb conjugation.

Example:
$ python2.7 conjugate.py go 3rg
goes
"""

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('verb', type=str, help='verb to be conjugated')
    parser.add_argument('conj', type=str, help="conjugation form ('3sg', etc.)")
    args = parser.parse_args()
    print conjugate(args.verb, args.conj)
