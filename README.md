# Generating Questions and Multiple-Choice Answers using Semantic Analysis of Texts
This repository provides source code used for our
[COLING 2016 paper](http://junaraki.net/pubs/coling2016-mcq.pdf) titled
``Generating Questions and Multiple-Choice Answers using Semantic Analysis of
Texts.''  It contains code for two question algorithms (QG1 and QG2) and
distractor generation.

## Requirements
- Python 3.6 (for QG1 only)
- Python 2.7
- Java 8 (for preprocessing)

For other requirements on python packages, please look at ```requirements.txt```.

## How to Run the Code
```
$ preprocess.sh
$ run.sh
```

## Citation
If you use the code, please cite our COLING 2016 paper:

```
@inproceedings{Araki2016Generating,
  author    = {Jun Araki and Dheeraj Rajagopal and Sreecharan Sankaranarayanan and Susan Holm and Yukari Yamakawa and Teruko Mitamura},
  title     = {Generating Questions and Multiple-Choice Answers using Semantic Analysis of Texts},
  booktitle = {Proceedings of COLING},
  pages     = {1125--1136},
  month     = {December},
  year      = {2016},
  address   = {Osaka, Japan},
}
```
