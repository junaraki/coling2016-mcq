# Download ProcessBank
wget http://ai2-website.s3.amazonaws.com/data/processbankdata.tar.gz
gunzip processbankdata.tar.gz
tar xvf processbankdata.tar
rm -f processbankdata.tar
cd processbankdata
tar xvfz bioprocess-bank-structures-train.tar.gz
tar xvfz bioprocess-bank-structures-test.tar.gz
tar xvfz bioprocess-bank-questions.tar.gz
cd ..

# For QG1 -- download and run Stanford CoreNLP
wget http://nlp.stanford.edu/software/stanford-corenlp-full-2018-02-27.zip
unzip stanford-corenlp-full-2018-02-27.zip
java -Xmx8g -cp "stanford-corenlp-full-2018-02-27/*" edu.stanford.nlp.pipeline.StanfordCoreNLP -annotators tokenize,ssplit,pos,lemma,ner,parse,dcoref -file processbankdata/test -extension .txt -threads 8 -outputFormat json -outputDirectory processbankdata/test

# For QG2 -- download the Stanford part-of-speech tagger
wget https://nlp.stanford.edu/software/stanford-postagger-full-2018-02-27.zip
unzip stanford-postagger-full-2018-02-27.zip
