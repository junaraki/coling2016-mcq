#!/usr/bin/env python

from pattern.en import pluralize
import argparse

"""
Python 2 code to use pattern.en to pluralize a noun.

Example:
$ python2.7 pluralize.py apple
apples
"""

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('noun', type=str, help='noun to be pluralized')
    args = parser.parse_args()
    print pluralize(args.noun)
