#!/usr/bin/env python

"""
Distractor generation.
"""

import Levenshtein
import random
import argparse
import pdb


def generate_distractors(input_file, output_file):
    question_file = open(input_file,"r")
    questions = question_file.readlines()
    questions = questions[1:]
    out_file = open(output_file,"w")
    out_file.write("question id\tdocument id\tdocument\tquestion\tanswer\toption 1\toption 2\toption 3\n")
    for line in questions:
        ann_inp = line.split("\t")[1]
        answer = line.strip("\n").split("\t")[4]
        ann_file = open("processbankdata/test/"+ann_inp+".ann","r") # Open .ann file obtained from the brat annotation tool
        contents = ann_file.readlines()
        ann_set = {} # Nodes of the event map
        ann_rel = {} # Relations used as edges in event map
        coref = {}
        for i in contents:
            if "T" in i.split("\t")[0]:
                ann_set[i.split("\t")[0]] = [i.split("\t")[1].split(" ")[0],i.split("\t")[1].split(" ")[1],i.split("\t")[1].split(" ")[2]] # Key is the name of the annotation, either Trigger or Entity and value is a list of name of the entity, start position and end position
            if "R" in i.split("\t")[0]:
                ann_rel[i.split("\t")[0]] = [i.split("\t")[1].split(" ")[0],i.split("\t")[1].split(" ")[1].split(":")[1],i.split("\t")[1].split(" ")[2].split(":")[1]]
            if "*" in i.split("\t")[0]:
                if i.split("\t")[1].split(" ")[0] == "Coref" or i.split("\t")[1].split(" ")[0] == "Same":
                    coref_set = i.strip("\n").split("\t")[1].split(" ")[1:]
                    coref_value = coref_set[0]
                    coref_keys = coref_set[1:]
                    for j in coref_keys:
                        coref[j] = coref_value

        # print ann_set
        # print ann_rel
        # print coref

        # Extraction of phrases containing triggers and two surrounding entities.
        # Sort the triggers and entities in order of appearance
        list1 = []
        list2 = []
        for i in ann_set.keys():
            list1.append(i) # List of 'T's
            list2.append(int(ann_set[i][1])) # List of Start Positions

        list1 = [x for (y,x) in sorted(zip(list2,list1))]

        # print list1 # Sorted list of 'T's in order of appearance
        phrase_list = [] # List of phrases
        txt_file = open("processbankdata/test/"+ann_inp+".txt","r")
        contents = txt_file.read()
        # print contents
        for i in ann_set.keys():
            if ann_set[i][0] == "Trigger":
                if list1.index(i) == 0: # Phrase starts with Trigger
                    if i not in coref.keys():
                        pos1 = int(ann_set[i][1])
                        if list1.index(i)+1 not in coref.keys():
                            pos2 = int(ann_set[list1[list1.index(i)+1]][2])
                            #if "," not in contents[pos1:pos2]: # Comma condition extreme case
                            phrase_list.append(contents[pos1:pos2])
                        elif list1.index(i)+1 in coref.keys():
                            pos2 = int(ann_set[list1[list1.index(i)+1]][1])-1
                            pos3 = int(ann_set[list1[list1.index(i)+1]][1])
                            pos4 = int(ann_set[list1[list1.index(i)+1]][2])
                            phrase = contents[pos1:pos2] + " " + contents[pos3:pos4]
                            phrase_list.append(phrase)
                    if i in coref.keys():
                        posa = int(ann_set[coref[i]][1])
                        posb = int(ann_set[coref[i]][2])
                        pos1 = int(ann_set[i][2])+1
                        if list1.index(i)+1 not in coref.keys():
                            pos2 = int(ann_set[list1[list1.index(i)+1]][2])
                            phrase = contents[posa:posb]+" "+contents[pos1:pos2]
                            phrase_list.append(phrase)
                        elif list1.index(i) + 1 in coref.keys():
                            pos2 = int(ann_set[list1[list1.index(i)+1]][1]) - 1
                            posc = int(ann_set[coref[list1[list1.index(i)+1]]][1])
                            posd = int(ann_set[coref[list1[list1.index(i)+1]]][2])
                            phrase = contents[posa:posb] + " " + contents[pos1:pos2] + " " + contents[posc:posd]
                            phrase_list.append(phrase)
                if list1.index(i) == len(list1)-1: # Phrase ends with Trigger
                    if i not in coref.keys():
                        if list1[list1.index(i)-1] not in coref.keys():
                            pos1 = int(ann_set[list1[list1.index(i)-1]][1])
                            pos2 = int(ann_set[i][2])
                            phrase_list.append(contents[pos1:pos2])
                        elif list1[list1.index(i)-1] in coref.keys():
                            pos1 = int(ann_set[coref[list1[list1.index(i)-1]]][1])
                            pos2 = int(ann_set[coref[list1[list1.index(i)-1]]][2])
                            pos3 = int(ann_set[list1[list1.index(i)-1]][2])+1
                            pos4 = int(ann_set[i][2])
                            phrase = contents[pos1:pos2] + " " + contents[pos3:pos4]
                            phrase_list.append(phrase)
                    if i in coref.keys():
                        if list1[list1.index(i)-1] not in coref.keys():
                            pos1 = int(ann_set[list1[list1.index(i)-1]][1])
                            pos2 = int(ann_set[i][1])-1
                            posa = int(ann_set[coref[i]][1])
                            posb = int(ann_set[coref[i]][2])
                            phrase = contents[pos1:pos2]+" "+contents[posa:posb]
                            phrase_list.append(phrase)
                        elif list1[list1.index(i)-1] in coref.keys():
                            pos1 = int(ann_set[coref[list1[list1.index(i)-1]]][1])
                            pos2 = int(ann_set[coref[list1[list1.index(i)-1]]][2])
                            pos3 = int(ann_set[list1[list1.index(i)-1]][2])+1
                            pos4 = int(ann_set[i][1]) - 1
                            pos5 = int(ann_set[coref[i]][1])
                            pos6 = int(ann_set[coref[i]][2])
                            phrase = contents[pos1:pos2] + " " + contents[pos3:pos4] + " " + contents[pos5:pos6]
                            phrase_list.append(phrase)
                else: # Trigger phrase is surrounded by entities
                    if i in coref.keys():
                        if list1[list1.index(i)-1] in coref.keys():
                            if list1[list1.index(i)+1] in coref.keys():
                                pos1 = int(ann_set[coref[list1[list1.index(i)-1]]][1])
                                pos2 = int(ann_set[coref[list1[list1.index(i)-1]]][2])

                                pos3 = int(ann_set[list1[list1.index(i)-1]][2])+1
                                pos4 = int(ann_set[i][1])-1

                                pos5 = int(ann_set[coref[i]][1])
                                pos6 = int(ann_set[coref[i]][2])

                                pos7 = int(ann_set[i][2])+1
                                pos8 = int(ann_set[list1[list1.index(i)+1]][1])-1

                                pos9 = int(ann_set[coref[list1[list1.index(i)+1]]][1])
                                pos10 = int(ann_set[coref[list1[list1.index(i)+1]]][2])
                                phrase = " ".join([contents[pos1:pos2],contents[pos3:pos4],contents[pos5:pos6],contents[pos7:pos8],contents[pos9:pos10]])
                                phrase_list.append(phrase)
                            elif list1[list1.index(i)+1] not in coref.keys():
                                pos1 = int(ann_set[coref[list1[list1.index(i)-1]]][1])
                                pos2 = int(ann_set[coref[list1[list1.index(i)-1]]][2])

                                pos3 = int(ann_set[list1[list1.index(i)-1]][2])+1
                                pos4 = int(ann_set[i][1])-1

                                pos5 = int(ann_set[coref[i]][1])
                                pos6 = int(ann_set[coref[i]][2])

                                pos7 = int(ann_set[i][2])+1
                                pos8 = int(ann_set[list1[list1.index(i)+1]][1])-1

                                pos9 = int(ann_set[list1[list1.index(i)+1]][1])
                                pos10 = int(ann_set[list1[list1.index(i)+1]][2])
                                phrase = " ".join([contents[pos1:pos2],contents[pos3:pos4],contents[pos5:pos6],contents[pos7:pos8],contents[pos9:pos10]])
                                phrase_list.append(phrase)
                        elif list1[list1.index(i)-1] not in coref.keys():
                            if list1[list1.index(i)+1] in coref.keys():
                                pos1 = int(ann_set[list1[list1.index(i)-1]][1])
                                pos2 = int(ann_set[list1[list1.index(i)-1]][2])

                                pos3 = int(ann_set[list1[list1.index(i)-1]][2])+1
                                pos4 = int(ann_set[i][1])-1

                                pos5 = int(ann_set[coref[i]][1])
                                pos6 = int(ann_set[coref[i]][2])

                                pos7 = int(ann_set[i][2])+1
                                pos8 = int(ann_set[list1[list1.index(i)+1]][1])-1

                                pos9 = int(ann_set[coref[list1[list1.index(i)+1]]][1])
                                pos10 = int(ann_set[coref[list1[list1.index(i)+1]]][2])
                                phrase = " ".join([contents[pos1:pos2],contents[pos3:pos4],contents[pos5:pos6],contents[pos7:pos8],contents[pos9:pos10]])
                                phrase_list.append(phrase)
                            elif list1[list1.index(i)+1] not in coref.keys():
                                pos1 = int(ann_set[list1[list1.index(i)-1]][1])
                                pos2 = int(ann_set[list1[list1.index(i)-1]][2])

                                pos3 = int(ann_set[list1[list1.index(i)-1]][2])+1
                                pos4 = int(ann_set[i][1])-1

                                pos5 = int(ann_set[coref[i]][1])
                                pos6 = int(ann_set[coref[i]][2])

                                pos7 = int(ann_set[i][2])+1
                                pos8 = int(ann_set[list1[list1.index(i)+1]][1])-1

                                pos9 = int(ann_set[list1[list1.index(i)+1]][1])
                                pos10 = int(ann_set[list1[list1.index(i)+1]][2])
                                phrase = " ".join([contents[pos1:pos2],contents[pos3:pos4],contents[pos5:pos6],contents[pos7:pos8],contents[pos9:pos10]])
                                phrase_list.append(phrase)
                    if i not in coref.keys():
                        if list1[list1.index(i)-1] in coref.keys():
                            if list1[list1.index(i)+1] in coref.keys():
                                pos1 = int(ann_set[coref[list1[list1.index(i)-1]]][1])
                                pos2 = int(ann_set[coref[list1[list1.index(i)-1]]][2])

                                pos3 = int(ann_set[list1[list1.index(i)-1]][2])+1
                                pos4 = int(ann_set[i][1])-1

                                pos5 = int(ann_set[i][1])
                                pos6 = int(ann_set[i][2])

                                pos7 = int(ann_set[i][2])+1
                                pos8 = int(ann_set[list1[list1.index(i)+1]][1])-1

                                pos9 = int(ann_set[coref[list1[list1.index(i)+1]]][1])
                                pos10 = int(ann_set[coref[list1[list1.index(i)+1]]][2])
                                phrase = " ".join([contents[pos1:pos2],contents[pos3:pos4],contents[pos5:pos6],contents[pos7:pos8],contents[pos9:pos10]])
                                phrase_list.append(phrase)
                            elif list1[list1.index(i)+1] not in coref.keys():
                                pos1 = int(ann_set[coref[list1[list1.index(i)-1]]][1])
                                pos2 = int(ann_set[coref[list1[list1.index(i)-1]]][2])

                                pos3 = int(ann_set[list1[list1.index(i)-1]][2])+1
                                pos4 = int(ann_set[i][1])-1

                                pos5 = int(ann_set[i][1])
                                pos6 = int(ann_set[i][2])

                                pos7 = int(ann_set[i][2])+1
                                pos8 = int(ann_set[list1[list1.index(i)+1]][1])-1

                                pos9 = int(ann_set[list1[list1.index(i)+1]][1])
                                pos10 = int(ann_set[list1[list1.index(i)+1]][2])
                                phrase = " ".join([contents[pos1:pos2],contents[pos3:pos4],contents[pos5:pos6],contents[pos7:pos8],contents[pos9:pos10]])
                                phrase_list.append(phrase)
                        elif list1[list1.index(i)-1] not in coref.keys():
                            if list1[list1.index(i)+1] in coref.keys():
                                pos1 = int(ann_set[list1[list1.index(i)-1]][1])
                                pos2 = int(ann_set[list1[list1.index(i)-1]][2])

                                pos3 = int(ann_set[list1[list1.index(i)-1]][2])+1
                                pos4 = int(ann_set[i][1])-1

                                pos5 = int(ann_set[i][1])
                                pos6 = int(ann_set[i][2])

                                pos7 = int(ann_set[i][2])+1
                                pos8 = int(ann_set[list1[list1.index(i)+1]][1])-1

                                pos9 = int(ann_set[coref[list1[list1.index(i)+1]]][1])
                                pos10 = int(ann_set[coref[list1[list1.index(i)+1]]][2])
                                phrase = " ".join([contents[pos1:pos2],contents[pos3:pos4],contents[pos5:pos6],contents[pos7:pos8],contents[pos9:pos10]])
                                phrase_list.append(phrase)
                            elif list1[list1.index(i)+1] not in coref.keys():
                                pos1 = int(ann_set[list1[list1.index(i)-1]][1])
                                pos2 = int(ann_set[list1[list1.index(i)-1]][2])

                                pos3 = int(ann_set[list1[list1.index(i)-1]][2])+1
                                pos4 = int(ann_set[i][1])-1

                                pos5 = int(ann_set[i][1])
                                pos6 = int(ann_set[i][2])

                                pos7 = int(ann_set[i][2])+1
                                pos8 = int(ann_set[list1[list1.index(i)+1]][1])-1

                                pos9 = int(ann_set[list1[list1.index(i)+1]][1])
                                pos10 = int(ann_set[list1[list1.index(i)+1]][2])
                                phrase = " ".join([contents[pos1:pos2],contents[pos3:pos4],contents[pos5:pos6],contents[pos7:pos8],contents[pos9:pos10]])
                                phrase_list.append(phrase)
        comma_list = []
        for phrase in phrase_list:
            if "," in phrase:
                comma_list.append(phrase)
        for comma in comma_list:
            phrase_list.remove(comma)
        phrase_list_2 = []
        for phrase in phrase_list:
            phrase_list_2.append("\""+phrase+"\"")
        phrase_list = phrase_list_2
        max = 0
        max_phrase = ""
        for phrase in phrase_list:
            if Levenshtein.distance(answer,phrase) > max:
                max = Levenshtein.distance(answer,phrase)
                max_phrase = phrase
        try:
            phrase_list.remove(max_phrase)
        except:
            pass
        random.shuffle(phrase_list)
        # print line.strip("\n")+",".join(phrase_list[:3])+"\n"
        out_file.write(line.rstrip()+"\t"+"\t".join(phrase_list[:3])+"\n")
    out_file.close()


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--input', type=str,
                        help='input (question) file')
    parser.add_argument('--output', type=str,
                        help='output file')
    args = parser.parse_args()

    generate_distractors(args.input, args.output)
