#! /usr/bin/env python

from os import listdir
from os.path import isfile, join

def files_from_folder(mypath):
	'''
	Get all files from a designated folder path
	'''
	onlyfiles = [ f for f in listdir(mypath) if isfile(join(mypath,f)) ]
	onlyfiles = [x for x in onlyfiles if ".DS_Store" not in x]
	return onlyfiles
