
# coding: utf-8

# """
# This contains the code for QG2 for the paper
# http://www.aclweb.org/anthology/C16-1107
# 
# Running the code:
# python qg2.py
# 
# Output:
# The results are stored in output_qg2.tsv
# """

# In[1]:

from text_utils import * 
from folder_utils import *
import xml
from bs4 import BeautifulSoup
from nltk.tag import StanfordPOSTagger
import nltk
from operator import itemgetter
import networkx as nx
import itertools


# In[2]:

from collections import Counter
from itertools import imap


# In[3]:

import random


# - Download the POS Tagger from Stanford
#     - Use path in STANFORD_POS_TAGGER_DIR
# - Download the Process Bank Data
#     - Use path in PROCESS_BANK_DATA_DIR

# In[5]:

STANFORD_POS_TAGGER_DIR = 'stanford-postagger-full-2018-02-27/'
PROCESS_BANK_DATA_DIR = 'processbankdata'

stanford_dir = STANFORD_POS_TAGGER_DIR
modelfile = stanford_dir + 'models/english-bidirectional-distsim.tagger'
jarfile = stanford_dir + 'stanford-postagger.jar'

st = StanfordPOSTagger(model_filename=modelfile, path_to_jar=jarfile)

DATA_FOLDER = PROCESS_BANK_DATA_DIR + "/qa/"
TRAIN_FOLDER = PROCESS_BANK_DATA_DIR + "/train/"
files_xml = [ x[x.find("p")+1:x.find(".ann")] for x in files_from_folder(TRAIN_FOLDER) if "ann" in x]


# In[6]:

def remove_tags(text):
    return ''.join(xml.etree.ElementTree.fromstring(text).itertext())

def filter_text(text):
    text = remove_tags(text)
    text = text.strip()
    text = text.replace("\n","")
    return text


# In[7]:

def parse_data_to_table(file_name):
    with open(file_name) as file_content:
        triggers = {}
        relationships = []
        
        rel_lines = [] ## its here because something weird is happening when looped twice
        
        for line in file_content:
            split_line = line.replace("\n","").split("\t")
            content_split = split_line[1].split(" ")
            
            if split_line[0][0] == "T":
                triggers[split_line[0]] = {}
                triggers[split_line[0]]["name"] =  split_line[2]
                triggers[split_line[0]]["type"] = content_split[0]
            
            else: 
                rel_lines.append(line)
                
        for line in rel_lines:
            split_line = line.replace("\n","").split("\t")
            ## If its a relation
            if split_line[0][0] == "R":
                dict_rel = {}
                rel_triple = split_line[1].split(" ")
                
                arg_1_index = rel_triple[1].split(":")[1]
                dict_rel["arg_1"] = triggers[arg_1_index]["name"]
                
                arg_2_index = rel_triple[2].split(":")[1]
                dict_rel["arg_2"] = triggers[arg_2_index]["name"]
                
                dict_rel["relationship"] = rel_triple[0]
                
                relationships.append(dict_rel)
                
            ## Coref link
            if split_line[0][0] == "*":
                coref_dict = {}
                
                coref_triple = split_line[1].split(" ")
                coref_dict["arg_1"] = coref_triple[1]
                coref_dict["arg_2"] = coref_triple[2]
                coref_dict["relationship"] = coref_triple[0]
                
                relationships.append(coref_dict)
      
    return triggers, relationships


# In[8]:

def check_edge_nodes(G, list_nodes):
    output_edges = []
    node_pairs = list(itertools.combinations(list_nodes, 2))
    for node_pair in node_pairs:
        if G.has_edge(node_pair[0], node_pair[1]):
            t = (node_pair[0], node_pair[1], G[node_pair[0]][node_pair[1]])
            output_edges.append(t)
    return output_edges


# ## Some files don't exist

# In[9]:

patterns = []
document_ranks = {}

ignore_ques_words = ["that"]

q_candidates = []

for file_name in files_xml:
    #print file_name
    try:
        data = open(DATA_FOLDER + file_name, "r").read()
    except:
        print file_name, "Not Found"
        continue
    xml_parse = BeautifulSoup(data, "html.parser")
    questions = [filter_text(str(x)) for x in xml_parse.find_all("q")]
    #text = open(TRAIN_FOLDER + "p" + file_name + ".txt", "r").read()
   
    output_triggers, output_rels = parse_data_to_table(TRAIN_FOLDER + "p" + file_name + ".ann")
    
    event_triggers = {}
    for key, value in output_triggers.items():
        event_triggers[value["name"]] = value["type"]

    
    G = nx.Graph()
    
    for sub_dict in output_rels:
        G.add_edge(sub_dict["arg_1"], sub_dict["arg_2"], label = sub_dict["relationship"])
        
    G_nodes = G.nodes()
        
    for question in questions:
        check_keys = []
        q_triggers = []
        modified_question = question
        i = 0
        for key in event_triggers.keys():
            if key in modified_question:
                i += 1
                modified_question = modified_question.replace(key, event_triggers[key])   #+"_"+str(i))
                #print modified_question, "(" , key , ")"
                q_triggers.append(event_triggers[key])
                check_keys.append(key)
        valid_keys = list(set(check_keys) & set(G_nodes))
          
        if len(valid_keys) == 1:
            t = (modified_question, check_keys, q_triggers,  [])
            q_candidates.append(t)
        
        if len(valid_keys) > 1:
            relation_check = check_edge_nodes(G, check_keys)
            #print relation_check
            if relation_check != [] :
                t = (modified_question, check_keys, q_triggers, relation_check)
                q_candidates.append(t)
    


# In[10]:

questions_rel = [x for x in q_candidates if len(x[1]) ==2 and x[3] != []]


# In[11]:

questions_arranged_by_label = {}

for q in questions_rel:
    label = q[3][0][2]["label"]
    t = ["1_"+label, q[2][0], q[2][1]]
    key_ques = tuple(sorted(t))
    
    if q[0].lower().startswith("wh"):
        if key_ques in questions_arranged_by_label.keys():
            questions_arranged_by_label[key_ques].append(q[0])
        else:
            questions_arranged_by_label[key_ques] = [q[0]]


# ## Removing all the specific question for better precision
# 
# ## Changes to existing questions for minor linguistic variation

# In[12]:

## DELETIONS 
questions_arranged_by_label[('1_Theme', 'Entity', 'Trigger')].remove('What color are colonies that can Trigger Entity?')
questions_arranged_by_label[('1_Theme', 'Entity', 'Trigger')].remove('What is eventually synthesized because the bacterial host cell will Trigger Entity?')
questions_arranged_by_label[('1_Theme', 'Entity', 'Trigger')].remove('What would happen if new layers of sediment did not Trigger Entity?')
questions_arranged_by_label[('1_Theme', 'Entity', 'Trigger')].remove('What Trigger Entity in the plasma membranes?')
questions_arranged_by_label[('1_Result', 'Entity', 'Trigger')].remove('What would happen if hemoglobin molecules did not Trigger into Entity?')
questions_arranged_by_label[('1_Theme', 'Entity', 'Trigger')].remove('What happens if the mutations Trigger Entity?')
questions_arranged_by_label[('1_Result', 'Entity', 'Trigger')].remove('What Trigger the supply of Entity?')
questions_arranged_by_label[('1_Loc', 'Entity', 'Trigger')].remove('What is the result of gasses being Trigger in Entity?')

## CHANGES
questions_arranged_by_label[('1_Cause', 'Trigger', 'Trigger')] = ['What is caused by Trigger and Trigger together?']
questions_arranged_by_label[('1_Theme', 'Entity', 'Trigger')] = ['What is caused by Trigger in Entity?', 'What would happen without Trigger of Entity?', 'What causes Trigger of Entity?', 
                                                                 'What entity Trigger Entity?', 'What would happen if Entity were not Trigger?', 
                                                                 'what is required for the Trigger of Entity?', 'What helped Trigger Entity?',  
                                                                 'What is Trigger with Entity?', 'What is necessary for Trigger of Entity?', 
                                                                 'What would happen without Entity Trigger?', 'What do Entity Trigger produce?', 'What causes an Trigger Entity?', 
                                                                 'When Entity is Trigger, what is produced?', 'What would happen without the Trigger of Entity?', 'What happens because Entity Trigger?',
                                                                 'What would happen without the Trigger of Entity?', 'What directly causes Trigger of Entity?']


questions_arranged_by_label[('1_Result', 'Entity', 'Trigger')] = ['What is required to Trigger Entity?', 'What event should occur before the Trigger of Entity?']
questions_arranged_by_label[('1_Loc', 'Entity', 'Trigger')] = ['What was Trigger in Entity?', 'What Trigger Entity?']


for key, value in questions_arranged_by_label.iteritems():
    print key, value


# ## GENERATING DATA FROM THE TEST FOLDER

# - Output is stored in output_qg2.tsv

# In[16]:

## testing phase
TEST_FOLDER = PROCESS_BANK_DATA_DIR + "/test/"
test_files = [x[:x.index(".")] for x in files_from_folder(TEST_FOLDER)]


allowed_rels = ["Result", "Super", "Source", "Dest", "Theme", "Loc", "Cause"]
not_allowed_pronouns = [" it", " they ", " them"]

output_file = open("output_qg2.tsv", "w")

q_number = 1

for file_name in test_files:
    file_contents = open(TEST_FOLDER + file_name + ".txt", "r").read()
    triggers_raw, relationships_raw = parse_data_to_table(TEST_FOLDER + file_name + ".ann")
    triggers = {}
    for value in triggers_raw.values():
        triggers[value["name"]] = value["type"]

    #print >> f_out, file_contents
    rand_rel_nums =  random.sample(range(0, len(relationships_raw)-1), 3)
    
    for rand_num in rand_rel_nums:
        item = relationships_raw[rand_num]
        if item["relationship"] in allowed_rels:
            t = ["1_"+item["relationship"], triggers[item['arg_1']], triggers[item['arg_2']]]
            
            sorted_t = tuple(sorted(t))
            question_patterns = questions_arranged_by_label[sorted_t]
            
            if len(question_patterns) > 2:
                random_numbers =  random.sample(range(0, len(question_patterns)-1), 2)
                #print random_numbers
            else:
                random_numbers = range(len(question_patterns))
            
            for i, ques_pattern in enumerate(question_patterns):
                    if i in random_numbers:
                        #if item['arg_1'] == item['arg_2'] :
                            #pass
                        generated_ques = ques_pattern.replace(triggers[item['arg_1']], item['arg_1'] , 1)
                        generated_ques = generated_ques.replace(triggers[item['arg_2']], item['arg_2'], 1) 
                        
                        if any(imap(generated_ques.__contains__, not_allowed_pronouns)):
                            continue
                        
                        print >> output_file, q_number, "\t", file_name, "\t", file_contents.strip(), "\t", generated_ques
                        q_number += 1

output_file.close()
            



