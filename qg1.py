#!/usr/bin/env python

from subprocess import Popen, PIPE
from nltk.corpus import wordnet as wn
import argparse
import os
import json
import random
import sys
import pdb

"""
Question generation from the ProcessBank corpus.
"""

# Possible types of questions to be generated
QTYPES = ['evcoref', 'encoref', 'pp']


class Question(object):
    def __init__(self):
        self.qid = None  # question ID
        self.text = None  # question text
        self.doc = None  # a document where this question comes from
        self.answer = None  # correct answer
        self.distractors = []  # distrators
        self.grammaticality = None  # automated evaluation of question's grammaticality
        self.difficulty = None  # automated evaluation of question's difficulty (# of inference steps)
        self.evm1 = None  # coreferent event mention 1
        self.evm2 = None  # coreferent event mention 2


class QuestionGenerator(object):
    def __init__(self, question_types, num_dists=0, no_dup=False):
        self.question_types = question_types
        self.num_dists = num_dists
        self.no_dup = no_dup

    def generate_questions(self, docs):
        self.questions = []
        self.question_texts = {}
        self.qid = 1  # question ID counter

        for doc in docs:
            if 'evcoref' in self.question_types:
                # for ec in doc.ann['event_corefs']:
                #     for i in range(len(ec)-1):
                #         ant_evm, ref_evm = ec[i], ec[i+1]
                #         self.generate_questions_from_event_coref(ant_evm, ref_evm)
                for ec in doc.ann['event_corefs']:
                    num_evms = len(ec.coref_evms)
                    for i in range(num_evms):
                        evm1 = ec.coref_evms[i]
                        for j in range(i+1, num_evms):
                            evm2 = ec.coref_evms[j]
                            self.generate_questions_from_event_coref(evm1, evm2)
            if 'encoref' in self.question_types:
                # for ec in doc.ann['entity_corefs']:
                #     for i in range(len(ec)-1):
                #         ant_enm, ref_enm = ec[i], ec[i+1]
                #         self.generate_questions_from_entity_coref(ant_enm, ref_enm)
                for ec in doc.ann['entity_corefs']:
                    num_enms = len(ec.coref_enms)
                    for i in range(num_enms):
                        enm1 = ec.coref_enms[i]
                        for j in range(i+1, num_enms):
                            enm2 = ec.coref_enms[j]
                            self.generate_questions_from_entity_coref(enm1, enm2)
            if 'pp' in self.question_types:
                for evm in doc.ann['event_mentions']:
                    print(evm.doc.doc_id, evm.text, file=sys.stderr)  # for log
                    self.generate_questions_from_paraphrase(evm)

    def generate_questions_from_event_coref(self, ant_evm, ref_evm):
        if ant_evm.sent == ref_evm.sent:
            return  # ignore event coreference in a single sentence

        if (not ant_evm.from_event_rels and not ref_evm.from_event_rels and
            not ant_evm.to_event_rels and not ref_evm.to_event_rels):
            return  # ignore coreference mentions without any event relations

        # generate questions based on coreferent event mentions
        self.generate_questions_from_event_relations(ant_evm, ref_evm)
        self.generate_questions_from_event_relations(ref_evm, ant_evm)

    def generate_questions_from_event_relations(self, evm1, evm2):
        for event_rel in evm1.from_event_rels:
            if event_rel.rel_type == 'Cause' or event_rel.rel_type == 'Enable':
                self.create_question_from_result_relation(
                    evm1, evm2, event_rel.to_evm)

        for role1, event_arg1 in evm1.args.items():
            if role1 == 'Result':
                self.create_question_from_result_relation(evm1, evm2, event_arg1)

        for role1, event_arg1 in evm1.args.items():
            if role1 == 'Agent':
                if any([role2 for role2 in evm2.args if role2 == 'Agent']):
                    continue

                if has_nominal_trigger(evm2):
                    # it is difficult to generate questions in this case
                    pass
                if has_verbal_trigger(evm2):
                    clause_tokens = convert_event_mention_to_clause_tokens(
                        evm2, ignore_role='Agent')
                    clause_begin = clause_tokens[0]['characterOffsetBegin']
                    clause_end = clause_tokens[-1]['characterOffsetEnd']
                    clause = evm2.doc.text[clause_begin:clause_end]
                    if clause.startswith(evm2.text):
                        clause = make_third_singular_phrase(clause, evm2)
                        question_text = "What {}?".format(clause)
                    else:
                        continue
                else:
                    continue

                if self.no_dup and question_text in self.question_texts:
                    # skip duplicate questions
                    continue
                else:
                    self.question_texts[question_text] = True

                q = create_question(self.qid, question_text, evm1.doc, event_arg1,
                                    evm1, evm2, self.num_dists)
                self.qid += 1
                self.questions.append(q)

        for event_rel in evm1.to_event_rels:
            if event_rel.rel_type == 'Cause' or event_rel.rel_type == 'Enable':
                if any([rel for rel in evm2.to_event_rels
                            if rel.rel_type == event_rel.rel_type]):
                    continue

                if has_nominal_trigger(evm2):
                    question_text = "What causes {}?".format(
                        normalize_nominal_event_mention(evm2))
                elif has_verbal_trigger(evm2):
                    clause_tokens = convert_event_mention_to_clause_tokens(evm2)
                    clause = evm2.doc.text[clause_tokens[0]['characterOffsetBegin']:clause_tokens[-1]['characterOffsetEnd']]
                    if not has_preceding_argument(evm2):
                        clause = make_infinitive_phrase(clause, evm2)
                        question_text = "What makes it happen to {}?".format(clause)
                    else:
                        question_text = "What makes it happen that {}?".format(clause)
                else:
                    continue

                if self.no_dup and question_text in self.question_texts:
                    # skip duplicate questions
                    continue
                else:
                    self.question_texts[question_text] = True

                q = create_question(self.qid, question_text, evm1.doc,
                                    event_rel.from_evm,
                                    evm1, evm2, self.num_dists)
                self.qid += 1
                self.questions.append(q)

    def create_question_from_result_relation(self, evm1, evm2, answer):
        if (any([rel for rel in evm2.from_event_rels
                 if rel.rel_type == 'Cause' or rel.rel_type == 'Enable']) or
            any([event_arg2 for event_arg2 in evm2.args
                 if event_arg2 == 'Result'])):
            return

        if has_nominal_trigger(evm2):
            question_text = "What is a result of {}?".format(
                normalize_nominal_event_mention(evm2))
        elif has_verbal_trigger(evm2):
            clause_tokens = convert_event_mention_to_clause_tokens(evm2)
            clause = evm2.doc.text[clause_tokens[0]['characterOffsetBegin']:clause_tokens[-1]['characterOffsetEnd']]
            if not has_preceding_argument(evm2):
                clause = make_progressive_clause(clause, evm2)
            question_text = "What happens when {}?".format(clause)
        else:
            return

        if self.no_dup and question_text in self.question_texts:
            # skip duplicate questions
            return
        else:
            self.question_texts[question_text] = True

        q = create_question(self.qid, question_text, evm1.doc,
                            answer, evm1, evm2, self.num_dists)
        self.qid += 1
        self.questions.append(q)

    def generate_questions_from_entity_coref(self, ant_enm, ref_enm):
        self.generate_questions_from_event_with_anaphora(ant_enm, ref_enm)
        self.generate_questions_from_event_with_anaphora(ref_enm, ant_enm)

    def generate_questions_from_event_with_anaphora(self, enm1, enm2):
        def create_what_question_from_subject(clause_tokens, enm1):
            begin_idx = None
            for idx in range(len(clause_tokens)):
                if idx < len(enm1.tokens):
                    continue
                if (clause_tokens[idx]['pos'].startswith('MD') or
                    clause_tokens[idx]['pos'].startswith('V')):
                    begin_idx = idx
                    break
            if begin_idx is None:  # this is unexpected
                return

            token_begin = clause_tokens[begin_idx]
            token_end = clause_tokens[-1]
            if token_begin['pos'].startswith('V'):
                verb_3sg = make_verb_third_singular(token_begin)
                begin_idx += 1
                token_text = verb_3sg + ' ' + \
                             doc.text[clause_tokens[begin_idx]['characterOffsetBegin']:token_end['characterOffsetEnd']]
            else:
                token_text = doc.text[token_begin['characterOffsetBegin']:token_end['characterOffsetEnd']]

            # this is a hack for p114 -- begin
            if '(' in token_text and ')' not in token_text:
                token_text = ''.join(token_text.split('('))
            # this is a hack for p114 -- end

            question_text = "What {}?".format(token_text)

            if self.no_dup and question_text in self.question_texts:
                # skip duplicate questions
                return
            else:
                self.question_texts[question_text] = True

            q = create_question(self.qid, question_text, enm1.doc,
                                enm2, enm1, enm2, 0)
            self.qid += 1
            self.questions.append(q)

        question_text = None
        doc = enm1.doc
        if is_anaphora(enm1.text) and not is_anaphora(enm2.text):
            evms = [evm for evm in enm1.doc.ann['event_mentions']
                    for arg in evm.args.values() if arg.has_same_span(enm1)]
            for evm in evms:
                if has_nominal_trigger(evm):
                    if has_preceding_argument(evm, first_arg_span=enm1,
                                              first_arg_role='Agent') and \
                       has_following_argument(evm):
                        clause_tokens = convert_event_mention_to_clause_tokens(
                            evm, res_ana=False)
                        create_what_question_from_subject(clause_tokens, enm1)

                elif has_verbal_trigger(evm):
                    # if has_preceding_argument(evm, first_arg_span=enm1):
                    if has_preceding_argument(evm, first_arg_span=enm1) and \
                       has_following_argument(evm):
                        clause_tokens = convert_event_mention_to_clause_tokens(
                            evm, res_ana=False)

                        # this is a hack for p100 -- begin
                        clause_tokens = [t for t in clause_tokens if t['originalText'] != '"']
                        # this is a hack for p100 -- end

                        create_what_question_from_subject(clause_tokens, enm1)

    def generate_questions_from_paraphrase(self, evm):
        def create_what_question_with_paraphrases(clause_tokens, first_arg, paraphrases):
            begin_idx = None
            for idx in range(len(clause_tokens)):
                if idx < len(first_arg.tokens):
                    continue
                token = clause_tokens[idx]
                if (token['pos'].startswith('MD') or
                    (token['pos'].startswith('V') and
                     any([t for t in evm.tokens if t == token]))):
                    begin_idx = idx
                    break
            if begin_idx is None:  # this is unexpected
                return

            for (token, syn) in paraphrases:
                if token not in clause_tokens[begin_idx:]:
                    continue

                token_begin = clause_tokens[begin_idx]
                token_end = clause_tokens[-1]
                if token_begin['pos'].startswith('V'):
                    verb_3sg = make_verb_third_singular(token_begin)
                    begin_idx += 1
                    token_text = verb_3sg + ' ' + \
                                 doc.text[clause_tokens[begin_idx]['characterOffsetBegin']:token_end['characterOffsetEnd']]
                    begin_idx -= 1
                else:
                    token_text = doc.text[token_begin['characterOffsetBegin']:token_end['characterOffsetEnd']]

                token_text = token_text.replace(token['originalText'], syn)
                question_text = "What {}?".format(token_text)

                if self.no_dup and question_text in self.question_texts:
                    # skip duplicate questions
                    return
                else:
                    self.question_texts[question_text] = True

                q = create_question(self.qid, question_text, evm.doc,
                                    evm, None, None, 0)
                self.qid += 1
                self.questions.append(q)

        doc = evm.doc
        if has_nominal_trigger(evm):
            if has_preceding_argument(evm, first_arg_role='Agent') and \
                has_following_argument(evm):
                clause_tokens = convert_event_mention_to_clause_tokens(
                    evm, res_ana=True)
                paraphrases = []
                for token in clause_tokens:
                    if token['pos'].startswith('N'):
                        syns = [s for s in get_synonyms(token['lemma'], wn.NOUN)
                                if '_' not in s]
                        if syns:
                            syn = syns[0]
                            if token['pos'].endswith('S'):  # NNS or NNPS
                                syn = pluralize(syn)
                            else:
                                syn = singularize(syn)
                            paraphrases.append((token, syn))

                        # for syn in syns:
                        #     if '_' in syn:
                        #         continue
                        #     if token.pos.endswith('S'):  # NNS or NNPS
                        #         syn = pluralize(syn)
                        #     else:
                        #         syn = singularize(syn)
                        #     paraphrases.append((token, syn))

                if paraphrases:
                    create_what_question_with_paraphrases(
                        clause_tokens, sorted(evm.args.values())[0], paraphrases)

        elif has_verbal_trigger(evm):
            if has_preceding_argument(evm, first_arg_role='Agent') and \
                has_following_argument(evm):
            # if has_preceding_argument(evm) and has_following_argument(evm):
                clause_tokens = convert_event_mention_to_clause_tokens(
                    evm, res_ana=True)
                paraphrases = []
                for token in clause_tokens:
                    if token['pos'].startswith('N'):
                        syns = [s for s in get_synonyms(token['lemma'], wn.NOUN)
                                if '_' not in s]
                        if syns:
                            syn = syns[0]
                            if token['pos'].endswith('S'):  # NNS or NNPS
                                syn = pluralize(syn)
                            else:
                                syn = singularize(syn)
                            paraphrases.append((token, syn))

                        # for syn in syns:
                        #     if '_' in syn:
                        #         continue
                        #     if token.pos.endswith('S'):  # NNS or NNPS
                        #         syn = pluralize(syn)
                        #     else:
                        #         syn = singularize(syn)
                        #     paraphrases.append((token, syn))

                if paraphrases:
                    create_what_question_with_paraphrases(
                        clause_tokens, sorted(evm.args.values())[0], paraphrases)


def get_synonyms(lemma, pos):
    synsets = wn.synsets(lemma, pos=wn.NOUN)
    if not synsets:
        return []
    return [lm.name() for lm in synsets[0].lemmas() if lm.name() != lemma]


def create_question(qid, qText, document, answer, evm1, evm2, num_dists):
    q = Question()
    q.qid = qid
    q.text = qText
    q.doc = document
    q.evm1 = evm1
    q.evm2 = evm2
    if answer is not None:
        if isinstance(answer, EventMention):
            if has_verbal_trigger(answer):
                q.answer = normalize_answer_verbal_event_mention(answer)
            else:
                q.answer = normalize_answer_text(answer.text)
        else:
            if not hasattr(answer, 'text'):
                pdb.set_trace()
            q.answer = answer.text
    if num_dists > 0:
        q.distractors = generate_distractors(evm1, evm2, num_dists=num_dists)
    # q.grammaticality = 1
    # if not has_preceding_argument(evm2):
    #     # event trigger doesn't probably have a subject
    #     q.grammaticality = 2
    # q.difficulty = 1
    return q

def lowercase(word):
    """Lowercase the first character of the given word."""
    if word[0:1].isupper():
        return word[0:1].lower() + word[1:]
    return word

def uppercase(word):
    """Uppercase the first character of the given word."""
    if word[0:1].islower():
        return word[0:1].upper() + word[1:]
    return word

def resolve_entity_coref(event_arg):
    enms = event_arg.doc.get(EntityMention, event_arg.begin, event_arg.end)
    if not enms:
        return event_arg
    en = enms[0].entity
    if en is None:
        return event_arg
    else:
        repEnm = en.representative_mention()
        if repEnm is None:
            en.entity_mentions[0]
        else:
            return repEnm

def is_anaphora(word):
    word = word.lower()
    return (word in ['it', 'its', 'they', 'their', 'this', 'that', 'these', 'those'] or
            word.startswith('this ') or word.startswith('that ') or
            word.startswith('these ') or word.startswith('those '))

def resolve_anaphora(event_arg):
    doc = event_arg.doc
    ec, enm = None, None
    for _ec in doc.ann['corefs'].values():
        for _enm in _ec:
            sent_idx = _enm['sentNum'] - 1
            token_start_idx = _enm['startIndex'] - 1
            token_end_idx = _enm['endIndex'] - 1
            tokens = doc.ann['sentences'][sent_idx]['tokens'][token_start_idx:token_end_idx]
            enm_begin = tokens[0]['characterOffsetBegin']
            enm_end = tokens[-1]['characterOffsetEnd']
            if enm_begin == event_arg.begin and enm_end == event_arg.end:
                ec = _ec
                enm = _enm
                break

    if ec is None:
        return event_arg

    if not is_anaphora(enm['text']):
        return event_arg

    # Find an anaphora
    if enm['isRepresentativeMention']:
        return event_arg
    else:
        for _enm in _ec:
            if _enm['isRepresentativeMention']:
                sent_idx = _enm['sentNum'] - 1
                token_start_idx = _enm['startIndex'] - 1
                token_end_idx = _enm['endIndex'] - 1
                tokens = doc.ann['sentences'][sent_idx]['tokens'][token_start_idx:token_end_idx]
                enm_begin = tokens[0]['characterOffsetBegin']
                enm_end = tokens[-1]['characterOffsetEnd']

                event_arg = EntityMention()
                event_arg.doc = doc
                event_arg.begin = enm_begin
                event_arg.end = enm_end
                event_arg.tokens = tokens
                find_head_word(event_arg)
                return event_arg

    return event_arg

def normalize_nominal_event_mention(evm):
    np = lowercase(evm.text)  # noun phrase
    if not (np.startswith('a ') or np.startswith('an ') or np.startswith('the ') or
            np.startswith('this ') or np.startswith('that ') or
            np.startswith('these ') or np.startswith('those ')):
        np = 'the ' + np
    return np

def convert_event_mention_to_clause_tokens(evm, ignore_role=None, res_ana=True):
    components = [evm]
    if ignore_role is None:
        components.extend([arg for arg in evm.args.values() if arg.sent == evm.sent])
    else:
        components.extend([arg for role, arg in evm.args.items()
                           if arg.sent == evm.sent and role != ignore_role])
    components.sort()

    # concatenate arguments and text segments beween them while resolving entity coreference
    begin, end = components[0].begin, components[-1].end
    doc = evm.doc
    tokens = []
    for component in components:
        if isinstance(component, EntityMention):
            curr_tokens = [token for sent in doc.ann['sentences'] for token in sent['tokens']
                           if token['characterOffsetBegin'] >= begin and
                           token['characterOffsetEnd'] <= component.begin]
            if curr_tokens:
                tokens.extend(curr_tokens)
            # enm = resolve_entity_coref(component)
            if res_ana:
                component = resolve_anaphora(component)
            tokens.extend(component.tokens)
            begin = component.end
    curr_tokens = [token for sent in doc.ann['sentences'] for token in sent['tokens']
                   if token['characterOffsetBegin'] >= begin and
                   token['characterOffsetEnd'] <= end]  # for last text segment
    if curr_tokens:
        tokens.extend(curr_tokens)

    return tokens

def make_progressive_clause(clause, evm):
    if len(evm.tokens) == 1:
        verb_prog = make_verb_progressive(evm.tokens[0]['lemma'])
        clause = verb_prog + clause[len(evm.text):]
    return lowercase(clause)

def make_third_singular_phrase(clause, evm):
    if len(evm.tokens) == 1:
        verb_3sg = make_verb_third_singular(evm.tokens[0])
        clause = verb_3sg + clause[len(evm.text):]
    return lowercase(clause)

def make_infinitive_phrase(clause, evm):
    if len(evm.tokens) == 1:
        clause = evm.tokens[0]['lemma'] + clause[len(evm.text):]
    return lowercase(clause)

def make_verb_third_singular(verb):
    if verb['pos'] == 'VBD':  # past-tense third singular
        return conjugate(verb['originalText'], '3sgp')
    else:  # present-tense third singular
        return conjugate(verb['originalText'], '3sg')

def make_verb_progressive(verb):
    return conjugate(verb, 'part')

def conjugate(verb, form):
    with Popen(['python2.7', 'conjugate.py', verb, form], stdout=PIPE) as proc:
        output = proc.stdout.read().decode('utf-8').strip()
    return output

def pluralize(noun):
    with Popen(['python2.7', 'pluralize.py', noun], stdout=PIPE) as proc:
        output = proc.stdout.read().decode('utf-8').strip()
    return output

def singularize(noun):
    with Popen(['python2.7', 'singularize.py', noun], stdout=PIPE) as proc:
        output = proc.stdout.read().decode('utf-8').strip()
    return output

def normalize_answer_verbal_event_mention(evm):
    components = [evm]
    components.extend(evm.args.values())
    components.sort()
    begin, end = components[0].begin, components[-1].end

    if components[0] == evm:
        # if an event mention starts with a verb trigger, the trigger
        # is converted to its progressive form.
        tokens = evm.tokens
        verb_prog = make_verb_progressive(tokens[0]['lemma'])
        answerText = '{} {}'.format(verb_prog, ' '.join([t.text for t in tokens[1:]]))
        return normalize_answer_text(answerText)
    else:
        return normalize_answer_text(evm.text)

def normalize_answer_text(text):
    answer = uppercase(text)
    if not answer.endswith('.'):
        answer = answer + '.'
    return answer

def has_preceding_argument(evm, first_arg_span=None, first_arg_role=None):
    if not evm.args:
        return False

    role, first_arg = sorted([(role, enm) for role, enm in evm.args.items()],
                             key=lambda x: x[1])[0]
    if first_arg_span is not None and first_arg_role is not None:
        return (first_arg.precedes(evm) and first_arg.has_same_span(first_arg_span)
                and role == first_arg_role)
    elif first_arg_span is not None:
        return (first_arg.precedes(evm) and first_arg.has_same_span(first_arg_span))
    elif first_arg_role is not None:
        return (first_arg.precedes(evm) and role == first_arg_role)
    else:
        return first_arg.precedes(evm)

def has_following_argument(evm):
    return sorted([enm for enm in evm.args.values()])[-1].follows(evm)

def has_nominal_trigger(evm):
    # If a head word is not decided, approximate it with the last word
    head_word = evm.tokens[-1] if evm.head_word is None else evm.head_word
    return head_word['pos'].startswith('N')

def has_verbal_trigger(evm):
    # If a head word is not decided, approximate it with the last word
    head_word = evm.tokens[-1] if evm.head_word is None else evm.head_word
    return head_word['pos'].startswith('V')

def generate_distractors(evm1, evm2, num_dists=1):
    """
    Generate distractors randomly.
    """
    def same_sentence(evm1, evm2):
        if evm1.trigger.sentence == evm2.trigger.sentence:
            return True
        return False

    if num_dists == 0:
        return []

    assert evm1.doc == evm2.doc
    dists = []
    doc = evm1.doc
    for evm in doc.event_mentions:
        # First, find event mentions irrelevant to both evm1 and evm2.
        if evm == evm1 or evm == evm2:
            continue
        if same_sentence(evm, evm1) or same_sentence(evm, evm2):
            continue
        if any([event_rel for event_rel in evm1.from_event_rels if event_rel.to_evm == evm]):
            continue
        if any([event_rel for event_rel in evm1.to_event_rels if event_rel.from_evm == evm]):
            continue
        if any([event_rel for event_rel in evm2.from_event_rels if event_rel.to_evm == evm]):
            continue
        if any([event_rel for event_rel in evm2.to_event_rels if event_rel.from_evm == evm]):
            continue
        dists.append(evm)
    if len(dists) < num_dists:
        # Second, find sentences that do not involve any event mentions
        for sent in doc.sentences:
            if not any([trig for trig in doc.event_triggers if trig.covered_by(sent)]):
                dists.append(sent)
    if len(dists) < num_dists:
        # Third, find entity mentions
        num = max(len(doc.entity_mentions), num_dists - len(dists))
        dists.extend(random.sample(doc.entity_mentions, num))

    distTexts = [normalize_answer_text(distSpan.text)
                 for distSpan in random.sample(dists, num_dists)]
    if len(dists) < num_dists:
        for i in range(len(dists), num_dists):
            distTexts.append(None)
    return distTexts

def output_questions(output_file, questions, num_dists=0):
    """
    Write questions into the given file.

    Input:
    output_file (str) :: output file
    questions (list) :: a list of questions
    num_dists (int) :: the number of distractors
    """
    fout = open(output_file, 'w')
    header = ['question id', 'document id', 'document', 'question', 'answer']
    # header = ['question id', 'document id', 'document', 'grammaticality',
    #           'difficulty (# of inference steps)', 'question']
    # if num_dists > 0:
    #     for i in range(num_dists+1):
    #         header.append('answer choice ({})'.format(i+1))
    # header.extend(['correct answer', 'coref-evm1', 'coref-evm2'])
    fout.write('\t'.join(header))
    fout.write('\n')

    for q in questions:
        items = [str(q.qid), q.doc.doc_id, q.doc.text.strip(), q.text, q.answer]
        # items = [str(q.qid), q.doc.annId, q.doc.text.strip(),
        #          str(q.grammaticality), str(q.difficulty), q.text]
        # if num_dists == 0:
        #     items.append(q.answer)
        # else:
        #     answers = [q.answer]
        #     answers.extend(q.distractors)
        #     random.shuffle(answers)
        #     correctAnswerIdx = answers.index(q.answer)
        #     for i, answer in enumerate(answers):
        #         items.append(answer)
        #     items.append(str(correctAnswerIdx+1))
        # items.append(q.evm1.text)
        # items.append(q.evm2.text)
        fout.write('\t'.join(items))
        fout.write('\n')
    fout.close()

class Document(object):
    def __init__(self):
        self.doc_id = None
        self.text = None

class Event(object):  # event coreference cluster
    def __init__(self):
        self.coref_evms = []

class EventMention(object):
    def __init__(self):
        self.doc = None
        self.evm_id = None
        self.begin = None
        self.end = None
        self.sent = None
        self.tokens = []
        self.head_word = None
        self.args = {}
        self.from_event_rels = []
        self.to_event_rels = []
        self.event = None

    def __repr__(self):
        return 'EventMention(id={},begin={},end={},text={})'.format(
            self.evm_id, self.begin, self.end, self.text)

    @property
    def text(self):
        return self.doc.text[self.begin:self.end]

    def __lt__(self, other):
        return self.end < other.begin

class EventRelation(object):
    def __init__(self):
        self.doc = None
        self.rel_type = None
        self.from_evm = None
        self.to_evm = None

class Entity(object):  # entity coreference cluster
    def __init__(self):
        self.coref_enms = []

class EntityMention(object):
    def __init__(self):
        self.doc = None
        self.enm_id = None
        self.begin = None
        self.end = None
        self.sent = None
        self.tokens = []
        self.head_word = None
        self.entity = None

    def __repr__(self):
        return 'EntityMention(id={},begin={},end={},text={})'.format(
            self.enm_id, self.begin, self.end, self.text)

    @property
    def text(self):
        return self.doc.text[self.begin:self.end]

    def __lt__(self, other):
        return self.end < other.begin

    def precedes(self, other):
        return self.end < other.begin

    def follows(self, other):
        return self.begin > other.end

    def has_same_span(self, other):
        return (self.begin == other.begin and self.end == other.end)

def is_dependency_ancestor(token1, token2):
    """
    Returns true if token1 is a dependency ancestor of token2.
    """
    # Breadth-first search implementation
    visited, queue = set(), [token2]
    while queue:
        curr_token = queue.pop(0)
        if curr_token is token1:
            return True
        if 'head_deps' in curr_token:
            for head_dep in curr_token['head_deps']:
                head = head_dep['head']
                key = (head['characterOffsetBegin'], head['characterOffsetEnd'])
                if key not in visited:
                    visited.add(key)
                    queue.append(head)
    return False

def find_head_word(phrase):
    head_word_idx = get_head_word_index(phrase.tokens)
    if head_word_idx is not None:
        phrase.head_word = phrase.tokens[head_word_idx]

def get_head_word_index(tokens):
    """
    Input:
    tokens (list) :: list of tokens
    """
    if not tokens:
        return None
    num_tokens = len(tokens)
    if num_tokens == 1:
        return 0
    ordered_indexes = []  # indexes of tokens sorted with the head-dependency order
    for i in range(num_tokens):
        token1 = tokens[i]
        for j in range(i+1, num_tokens):
            token2 = tokens[j]
            if is_dependency_ancestor(token1, token2):
                if i in ordered_indexes:
                    index1 = ordered_indexes.index(i)
                    if j in ordered_indexes:
                        index2 = ordered_indexes.index(j)
                        if index1 > index2:  # need to reverse the order
                            ordered_indexes[index1] = j
                            ordered_indexes[index2] = i
                    else:
                        ordered_indexes.insert(index1 + 1, j)
                else:
                    if j in ordered_indexes:
                        ordered_indexes.insert(max(ordered_indexes.index(j) - 1, 0), i)
                    else:
                        ordered_indexes.append(i)
                        ordered_indexes.append(j)
            elif is_dependency_ancestor(token2, token1):
                if i in ordered_indexes:
                    index1 = ordered_indexes.index(i)
                    if j in ordered_indexes:
                        index2 = ordered_indexes.index(j)
                        if index1 < index2:  # need to reverse the order
                            ordered_indexes[index1] = j
                            ordered_indexes[index2] = i
                    else:
                        ordered_indexes.insert(max(index1 - 1, 0), j)
                else:
                    if j in ordered_indexes:
                        ordered_indexes.insert(ordered_indexes.index(j) + 1, i)
                    else:
                        ordered_indexes.append(j)
                        ordered_indexes.append(i)
    if len(ordered_indexes) > 1:
        return ordered_indexes[0]
    return None

def load_data(input_dir):
    docs = []
    for f in os.listdir(input_dir):
        if not f.endswith('.txt'):
            continue
        doc = Document()
        doc.doc_id = os.path.splitext(f)[0]

        input_file = os.path.join(input_dir, f)
        with open(input_file, 'r') as fin:
            doc.text = fin.read()
        corenlp_file = '{}.json'.format(input_file)
        with open(corenlp_file, 'r') as fin:
            doc.ann = json.load(fin)

        id2evms, id2enms = {}, {}  # local cache
        doc.ann['event_mentions'] = []
        doc.ann['entity_mentions'] = []
        doc.ann['event_corefs'] = []
        doc.ann['entity_corefs'] = []

        ann_file = os.path.join(input_dir, '{}.ann'.format(doc.doc_id))
        fin = open(ann_file, 'r')
        for line in fin:
            if not line.startswith('T'):
                continue
            ann_id, type_offsets, text = line.strip().split('\t')
            ann_type, begin, end = type_offsets.split(' ')
            assert doc.text[int(begin):int(end)] == text
            if ann_type == 'Trigger':
                evm = EventMention()
                evm.doc = doc
                evm.evm_id = ann_id
                evm.begin = int(begin)
                evm.end = int(end)
                for sent in doc.ann['sentences']:
                    sent_begin = sent['tokens'][0]['characterOffsetBegin']
                    sent_end = sent['tokens'][-1]['characterOffsetEnd']
                    if sent_begin <= evm.begin < evm.end <= sent_end:
                        # Found a sentence containing the trigger
                        evm.sent = sent
                        for token in sent['tokens']:
                            token_begin = token['characterOffsetBegin']
                            token_end = token['characterOffsetEnd']
                            if evm.begin <= token_begin < token_end <= evm.end:
                                evm.tokens.append(token)
                        num_tokens = len(sent['tokens'])
                        for dep in sent['basicDependencies']:
                            if dep['dep'] == 'ROOT':
                                continue
                            head_idx = dep['governor'] - 1
                            tail_idx = dep['dependent'] - 1
                            assert 0 <= head_idx < num_tokens
                            assert 0 <= tail_idx < num_tokens
                            head_token = sent['tokens'][head_idx]
                            tail_token = sent['tokens'][tail_idx]
                            dep['head'] = head_token
                            dep['tail'] = tail_token
                            if 'tail_deps' not in head_token:
                                head_token['tail_deps'] = []
                            head_token['tail_deps'].append(dep)
                            if 'head_deps' not in tail_token:
                                tail_token['head_deps'] = []
                            tail_token['head_deps'].append(dep)
                        break
                assert evm.sent is not None
                find_head_word(evm)
                doc.ann['event_mentions'].append(evm)
                id2evms[ann_id] = evm
            elif ann_type == 'Entity':
                enm = EntityMention()
                enm.doc = doc
                enm.enm_id = ann_id
                enm.begin = int(begin)
                enm.end = int(end)
                for sent in doc.ann['sentences']:
                    sent_begin = sent['tokens'][0]['characterOffsetBegin']
                    sent_end = sent['tokens'][-1]['characterOffsetEnd']
                    if sent_begin <= enm.begin < enm.end <= sent_end:
                        enm.sent = sent
                        for token in sent['tokens']:
                            token_begin = token['characterOffsetBegin']
                            token_end = token['characterOffsetEnd']
                            if enm.begin <= token_begin < token_end <= enm.end:
                                enm.tokens.append(token)
                        break
                assert enm.sent is not None
                find_head_word(enm)
                doc.ann['entity_mentions'].append(enm)
                id2enms[ann_id] = enm

        fin.seek(0)
        for line in fin:
            if not line.startswith('R'):
                continue
            rel_id, rels = line.strip().split('\t')
            rel_type, arg1, arg2 = rels.split(' ')
            _, ann_id1 = arg1.split(':')
            _, ann_id2 = arg2.split(':')
            if rel_type in ['Loc', 'Theme', 'Agent', 'Source', 'Dest', 'Result', 'Other']:
                # Argument relations
                evm = id2evms[ann_id1]
                enm = id2enms[ann_id2]
                evm.args[rel_type] = enm
            else:
                # Inter-event relations
                assert rel_type in ['Enable', 'Enable-or', 'Super', 'Cause', 'Cause-or',
                                    'Prevent', 'Prevent-or']
                event_rel = EventRelation()
                event_rel.rel_type = rel_type
                from_evm = id2evms[ann_id1]
                to_evm = id2evms[ann_id2]
                event_rel.from_evm = from_evm
                event_rel.to_evm = to_evm
                from_evm.from_event_rels.append(event_rel)
                to_evm.to_event_rels.append(event_rel)

        fin.seek(0)
        for line in fin:
            if not line.startswith('*'):
                continue
            equiv_type, rels = line.strip().split('\t')
            rels = rels.split(' ')
            assert rels[0] == 'Same' or rels[0] == 'Coref'
            if rels[0] == 'Same':
                event = Event()
                for i in range(1, len(rels)):
                    evm = id2evms[rels[i]]
                    event.coref_evms.append(evm)
                    evm.event = event
                doc.ann['event_corefs'].append(event)
            else:
                entity = Entity()
                for i in range(1, len(rels)):
                    enm = id2enms[rels[i]]
                    entity.coref_enms.append(enm)
                    enm.entity = entity
                doc.ann['entity_corefs'].append(entity)

        docs.append(doc)
        fin.close()

    return docs


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--input', type=str,
                        help='input directory (ProcessBank)')
    parser.add_argument('--output', type=str,
                        help='output file (TSV format)')
    parser.add_argument('--num-dists', type=int, default=0,
                        help='number of distractors')
    parser.add_argument('--no-dup', action='store_true',
                        help='do not allow for duplicated questions')
    parser.add_argument('--question-types', type=str, nargs='+',
                        default=QTYPES, choices=QTYPES,
                        help='types of questions to be generated')
    args = parser.parse_args()

    docs = load_data(args.input)
    print("Load {} documents.".format(len(docs)))
    qg = QuestionGenerator(args.question_types, num_dists=args.num_dists,
                           no_dup=args.no_dup)
    qg.generate_questions(docs)
    output_questions(args.output, qg.questions, num_dists=args.num_dists)
