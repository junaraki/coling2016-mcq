# Run QG1 -- generate questions using QG1
python3.6 qg1.py --input processbankdata/test --no-dup --output output_qg1.tsv

# Run QG2 -- generate questions using QG2
python2.7 qg2.py

# Run distractor generation
python2.7 dg.py --input output_qg1.tsv --output output_qg1_dist.tsv
